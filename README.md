# Sailfish OS for F(x)tec Pro¹

* master - [![pipeline status](https://gitlab.com/sailfishos-porters-ci/t5-ci/badges/master/pipeline.svg)](https://gitlab.com/sailfishos-porters-ci/t5-ci/commits/master)
* 3.2.1.20 - [![pipeline status](https://gitlab.com/sailfishos-porters-ci/t5-ci/badges/3.2.1.20/pipeline.svg)](https://gitlab.com/sailfishos-porters-ci/t5-ci/commits/3.2.1.20)

note that as of 2020-02-022 this is still being worked on, so do not expect a 3.2.1.20 tag quite yet.

This repository uses gitlab-ci to build the sailfish images for the F(x)tec Pro¹ (aka Pro1).

![F(x)tec Pro¹](https://www.fxtec.com/wp-content/uploads/2019/08/Keyboard-800.png "F(x)tec Pro¹")

## Device specifications

See also [https://www.fxtec.com/pro1/#specification](https://www.fxtec.com/pro1/#specification)

Basic   | Spec Sheet
-------:|:----------
CPU     | Octa-core 2.5 GHz Qualcomm Kryo V2
Chipset | Qualcomm Snapdragon 835, MSM8998
ROM     | 128GB 
RAM     | 6GB
Android | 9.0
Battery | 3200 mAh
Display | 2160x1080 pixels, 5.99
Rear Camera  | 12MP
Front Camera | 8 MP

# Files

[t5.env](https://gitlab.com/sailfishos-porters-ci/t5-ci/blob/master/t5.env) contains all the environment variables required for the process.
<!-- On every release, version number is changed in the master branch and a new tag is created with the release number to trigger the build process. -->

[Jolla-@RELEASE@-t5-@ARCH@.ks](https://gitlab.com/sailfishos-porters-ci/t5-ci/blob/master/Jolla-@RELEASE@-t5-@ARCH@.ks) file is the kickstart file which is used by PlatformSDK image, this files contains device specific repositories, Repositories can be changed by from devel to testing or vice versa.

[run-mic.sh](https://gitlab.com/sailfishos-porters-ci/t5-ci/blob/master/run-mic.sh) is a simple bash script, which executes the build.

# Download

[Download the latest build](https://gitlab.com/sailfishos-porters-ci/t5-ci/-/jobs?scope=finished)

# Source code

[https://github.com/sailfish-on-fxtecpro1/droid-config-t5](https://github.com/sailfish-on-fxtecpro1/droid-config-t5)

# Installation Instructions

[https://community.fxtec.com/topic/2467-community-build-sailfish-os-32-for-fxtec-pro1/](https://community.fxtec.com/topic/2467-community-build-sailfish-os-32-for-fxtec-pro1/)

# Wiki

[https://together.jolla.com/question/220410/wiki-fxtec-pro1-sailfish-os-tips-and-tricks/](https://together.jolla.com/question/220410/wiki-fxtec-pro1-sailfish-os-tips-and-tricks/)

# kudos

This README doc format is taken from [dumpling-ci](https://gitlab.com/sailfishos-porters-ci/dumpling-ci)
and [vince-ci](https://gitlab.com/sailfishos-porters-ci/vince-ci), thank you!